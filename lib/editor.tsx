import * as React from 'react';
import { EditorState, convertToRaw, convertFromRaw } from 'draft-js';
import Editor from 'draft-js-plugins-editor-wysiwyg';
import { DefaultDraftBlockRenderMap } from 'draft-js';
import createPlugins from './createPlugins';
import { Map } from 'immutable';

export interface IWysiwygEditorProps {
  progress: any;
  isDragging?: boolean;
  readOnly?: boolean;
  blockTypes?: any;
  value?: any;
  onChange?: any;
  percent?;
  any;
  uploading?: any;
  fileDrag?: any;
}

interface IWysiwygEditorState {
  readOnly?: boolean;
  fileDrag?: any;
  active?: boolean;
  editorState?: any;
  percent?: any;
}

export default class WysiwygEditor extends React.Component<IWysiwygEditorProps, IWysiwygEditorState> {
  batch = batch(200);
  plugins: any[] = null;
  editorState: any = null;
  blockRenderMap: any = null;
  unmounted: boolean = false;
  _raw = null;
  constructor(props) {
    super(props);
    this.plugins = createPlugins(props);
    this.editorState = props.value
      ? EditorState.push(EditorState.createEmpty(), convertFromRaw(props.value))
      : EditorState.createEmpty();
    this.blockRenderMap = DefaultDraftBlockRenderMap.merge(this.customBlockRendering(props));
    this.state = {};
  }

  componentWillUnmount() {
    this.unmounted = true;
  }

  shouldComponentUpdate(props, state) {
    if (this.props.value !== props.value && this._raw !== props.value) {
      this.editorState = !props.value
        ? EditorState.createEmpty()
        : EditorState.push(this.editorState, convertFromRaw(props.value));
      return true;
    } else if (
      this.state.active !== state.active ||
      this.state.readOnly !== state.readOnly ||
      this.state.editorState !== state.editorState
    ) {
      return true;
    } else if (
      this.props.readOnly !== props.readOnly ||
      this.props.fileDrag !== props.fileDrag ||
      this.props.uploading !== props.uploading ||
      this.props.percent !== props.percent
    ) {
      return true;
    }
    return false;
  }

  onChange = editorState => {
    if (this.unmounted) return;
    this.editorState = editorState;
    this.setState({ editorState: Date.now() });

    if (this.props.onChange) {
      this.batch(() => {
        this._raw = convertToRaw(editorState.getCurrentContent());
        this.props.onChange(this._raw, editorState);
      });
    }
  };

  focus = () => {
    this.refs.editor['focus']();
  };

  blockRendererFn = contentBlock => {
    const { blockTypes } = this.props;
    const type = contentBlock.getType();
    return blockTypes && blockTypes[type]
      ? {
          component: blockTypes[type]
        }
      : undefined;
  };

  customBlockRendering = props => {
    const { blockTypes } = props;
    var newObj = {
      paragraph: {
        element: 'div'
      },
      unstyled: {
        element: 'div'
      },
      'block-image': {
        element: 'div'
      },
      'block-table': {
        element: 'div'
      }
    };
    for (var key in blockTypes) {
      newObj[key] = {
        element: 'div'
      };
    }
    return Map(newObj);
  };

  render() {
    const { editorState } = this;
    const { isDragging, progress, readOnly } = this.props;

    return (
      <Editor
        readOnly={readOnly}
        editorState={editorState}
        plugins={this.plugins}
        blockRenderMap={this.blockRenderMap}
        blockRendererFn={this.blockRendererFn}
        onChange={this.onChange}
        ref="editor"
      />
    );
  }
}

const batch = (limit = 500) => {
  var _callback = null;
  return callback => {
    _callback = callback;
    setTimeout(() => {
      if (_callback === callback) {
        callback();
      }
    }, limit);
  };
};
